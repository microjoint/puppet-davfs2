#
class davfs2::install inherits davfs2 {
  package { 'davfs2':
    ensure => 'present',
  }
}
