#
class davfs2::config inherits davfs2 {
  file{'/etc/davfs2/davfs2.conf':
    ensure => file,
    source => "puppet:///modules/${module_name}/davfs2.conf",
  }
}
